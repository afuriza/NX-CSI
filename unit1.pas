unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ComCtrls,
  ExtCtrls, StdCtrls, BCToolBar, BGRABitmap, zlib, dynlibs;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Image1: TImage;
    OpenDialog1: TOpenDialog;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

  function ToCompressedStream(ABitmap: TBitmap): TStream; cdecl; external 'nx_csi';

  function ToDecompressedStream(AStream: TStream): TStream; cdecl; external 'nx_csi';

var
  Form1: TForm1;
  mHandle: TLibHandle;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
var
  mBM: TBGRABitmap;
  mStream: TStream;
  //mCBM: TBGRACustomBitmap;
begin
  if OpenDialog1.Execute then
  begin
    mBM := TBGRABitmap.create(OpenDialog1.FileName);
    //mbm := mBM.FilterGrayscale as TBGRABitmap;
    mStream := TMemoryStream.Create;
    mStream := ToCompressedStream(mBM.Bitmap);
    mStream.Position:=0;
    mStream := ToDecompressedStream(mStream);
    mStream.Position:=0;
    mbm.Free;
    Image1.Picture.Bitmap.LoadFromStream(mStream);
    mStream.Free;
    //mBM.Free;
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin

end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  mHandle := LoadLibrary('nx_csi.dll');
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  UnLoadLibrary(mHandle);
end;

end.
